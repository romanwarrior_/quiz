using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static int Count(this string s, char value)
    {
        int result = 0;

        foreach (char ch in s)
        {
            if (ch == value)
                result++;
        }
        return result;
    }
}
