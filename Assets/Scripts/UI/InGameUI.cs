using TMPro;
using UnityEngine;

public class InGameUI : MonoBehaviour
{
    [SerializeField] private GameObject gameResult;
    [SerializeField] private TextMeshProUGUI resultText;
    [SerializeField] private TextMeshProUGUI loadNextLevelText;

    private bool previousLevelResult;

    private readonly Color32 green = new Color32(17, 142, 34, 255);
    private readonly Color32 red = new Color32(142, 17, 17, 255);

    private void Start()
    {
        GameEvents.Instance.OnLevelCompleted.AddListener(OnLevelCompleted);
        GameEvents.Instance.OnGameFinished.AddListener(OnGameFinished);
    }

    private void OnLevelCompleted(bool win)
    {
        previousLevelResult = win;
        gameResult.SetActive(true);
        if (win)
        {
            resultText.text = "VICTORY";
            resultText.color = green;
            loadNextLevelText.text = "NEXT LEVEL";
        }
        else
        {
            resultText.text = "DEFEAT";
            resultText.color = red;
            loadNextLevelText.text = "RESTART";
        }
    }

    private void OnGameFinished()
    {
        gameResult.SetActive(true);
        resultText.text = "GAME COMPLETED";
        resultText.color = green;
        loadNextLevelText.text = "RESTART";
    }

    public void HandleNextLevelClick()
    {
        GameEvents.Instance.OnLoadLevelClicked?.Invoke(previousLevelResult);
        gameResult.SetActive(false);
    }
}
