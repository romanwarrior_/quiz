using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private LevelManagement levelManagement;
    [SerializeField] private GameObject attemptsText;

    private void Start()
    {
        attemptsText.SetActive(false);
    }

    public void StartNewGame()
    {
        levelManagement.StartNewGame();
        CloseMainMenu();
    }

    public void Continue()
    {
        levelManagement.Continue();
        CloseMainMenu();
    }

    public void Quit()
    {
        Application.Quit();
    }

    private void CloseMainMenu()
    {
        gameObject.SetActive(false);
        attemptsText.SetActive(true);
    }
}
