using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputHandler : MonoBehaviour
{
    [SerializeField] private new Camera camera;

    private bool inputEnabled;

    private void Start()
    {
        GameEvents.Instance.OnLevelCompleted.AddListener(DisableInput);
        GameEvents.Instance.OnLoadLevelClicked.AddListener(EnableInput);
        EnableInput();
    }

    private void Update()
    {
        if (!inputEnabled) return;

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(camera.ScreenToWorldPoint(Input.mousePosition), Vector3.forward);
            if (hit.collider!= null && hit.collider.TryGetComponent(out AlphabetButton button))
            {
                button.HandleClick();
            }
        }
    }

    private void EnableInput(bool _ = true)
    {
        inputEnabled = true;
    }

    private void DisableInput(bool _ = true)
    {
        inputEnabled = false;
    }
}
