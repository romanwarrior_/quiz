using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreCounter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;

    private FieldToSave<int> score = new FieldToSave<int>("score",0);

    private void Start()
    {
        GameEvents.Instance.OnScoreGained.AddListener(OnScoreGained);
        GameEvents.Instance.OnLevelCompleted.AddListener(OnLevelCompleted);
        GameEvents.Instance.OnGameFinished.AddListener(OnGameFinished);
        GameEvents.Instance.OnNewGameStarted.AddListener(ResetScore);
        UpdateUI();
    }

    private void ResetScore()
    {
        score.Value = 0;
        UpdateUI();
    }

    private void OnLevelCompleted(bool win)
    {
        if (!win)
        {
            score.Value=0;
            UpdateUI();
        }
    }

    private void OnGameFinished()
    {
        score.Value = 0;
    }

    private void OnScoreGained(int value)
    {
        score.Value += value;
        UpdateUI();
    }


    private void UpdateUI()
    {
        text.text = $"Score : {score.Value}";
    }

}
