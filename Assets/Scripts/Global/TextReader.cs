using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TextReader : MonoBehaviour
{
    [SerializeField] private DataInstaller dataInstaller;

    public List<string> Words => words;

    private List<string> words = new List<string>();

    private const string path = @"C:\Unity\MyOwn\Quiz\Assets\alice30.txt";
    private readonly string[] ignoredSymbols =
    {
        ",",")","(",".","!","?","0","1","2","3","4","5","6","7","8","9","-","_",";",":","\"","'s","'ll","`","'"
    };
    private readonly string[] ignoredWords =
    {
        "didn","don","ain","wouldn","couldn","shan","doesn","hadn","wasn","shouldn","isn","haven","needn","weren","mayn"
    };

    private void Awake()
    {
        StreamReader f = new StreamReader(path);
        while (!f.EndOfStream)
        {
            string s = f.ReadLine();

            FormatString(ref s);
            if (s == string.Empty) continue;

            string[] words = s.Trim().Split(' ');

            for (int i = 0; i < words.Length; i++)
            {
                if (words[i].Length >= dataInstaller.MinimalWordLength && !this.words.Contains(words[i]))
                    this.words.Add(words[i]);
            }
        }
        f.Close();
    }

    private void ReplaceSubstr(ref string str, string substr,string replacer)
    {
        while (str.Contains(substr)) { str = str.Replace(substr, replacer); }
    }

    private void FormatString(ref string s)
    {
        s = s.ToLower();

        while (s.Contains("  ")) { s = s.Replace("  ", " "); }

        foreach (string symbol in ignoredSymbols)
        {
            ReplaceSubstr(ref s, symbol, " ");
        }
        foreach (string symbol in ignoredWords)
        {
            ReplaceSubstr(ref s, symbol, string.Empty);
        }
    }
}
