using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldToSave<T>
{
    private string key;
    private bool isLoaded;
    private T defaultValue;
    private T value;

    public T Value
    {
        get
        {
            if (!isLoaded)
            {
                isLoaded = true;
                value = ValueGet();
            }

            return value;
        }

        set
        {
            this.value = value;
            SaveValue();
        }
    }

    public FieldToSave(string key, T defaultValue)
    {
        this.key = key;
        this.defaultValue = defaultValue;
    }

    private T ValueGet()
    {
        if (typeof(T) == typeof(int))
            return (T)(object)PlayerPrefs.GetInt(key, (int)(object)defaultValue);
        if (typeof(T) == typeof(float))
            return (T)(object)PlayerPrefs.GetFloat(key, (float)(object)defaultValue);
        if (typeof(T) == typeof(string))
            return (T)(object)PlayerPrefs.GetString(key, (string)(object)defaultValue);
        if (typeof(T) == typeof(bool))
            return (T)(object)(PlayerPrefs.GetInt(key, (bool)(object)defaultValue ? 1 : 0) == 1);

        return value;
    }

    private void SaveValue()
    {
        if (typeof(T) == typeof(int))
            PlayerPrefs.SetInt(key, (int)(object)value);
        if (typeof(T) == typeof(float))
            PlayerPrefs.SetFloat(key, (float)(object)value);
        if (typeof(T) == typeof(string))
            PlayerPrefs.SetString(key, (string)(object)value);
        if (typeof(T) == typeof(bool))
            PlayerPrefs.SetInt(key, (bool)(object)value? 1:0);
    }
}
