using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEvents : MonoBehaviour
{
    public UnityEvent<char> OnButtonClicked;
    public UnityEvent<bool> OnLevelCompleted;
    public UnityEvent<bool> OnLoadLevelClicked;
    public UnityEvent<int> OnScoreGained;
    public UnityEvent OnAttemptLost;
    public UnityEvent OnGameFinished;
    public UnityEvent OnNewGameStarted;

    #region Singleton
    private static GameEvents instance;

    public static GameEvents Instance
    {
        get
        {
            return instance ??= FindObjectOfType<GameEvents>();
        }
    }
    #endregion
}
