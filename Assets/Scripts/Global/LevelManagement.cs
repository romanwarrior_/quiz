using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManagement : MonoBehaviour
{
    [SerializeField] private LevelGenerator levelGenerator;

    private FieldToSave<int> lastLevelPassed = new FieldToSave<int>("lastLevelPassed", -1);

    private void Start()
    {
        GameEvents.Instance.OnLoadLevelClicked.AddListener(OnLoadLevelButtonCliciked);
    }

    public void StartNewGame()
    {
        lastLevelPassed.Value = -1;
        GameEvents.Instance.OnNewGameStarted?.Invoke();
        Continue();
    }

    public void Continue() 
    { 
        levelGenerator.GenerateLevel(lastLevelPassed.Value+1);
    }

    private void OnLoadLevelButtonCliciked(bool win)
    {
        if (win)
        {
            lastLevelPassed.Value++;
            if (lastLevelPassed.Value == levelGenerator.WordsCount-1)
                StartNewGame();
            else 
                Continue();
        }
        else
        {
            StartNewGame();
        }
    }
}
