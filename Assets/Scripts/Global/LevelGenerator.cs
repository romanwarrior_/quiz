using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private TextReader textReader;
    [SerializeField] private Word word;
    [SerializeField] private GameObject keyboard;

    public int WordsCount => wordsLeft.Count;

    private List<string> wordsLeft;

    private void Start()
    {
        wordsLeft = textReader.Words;
    }

    public void GenerateLevel(int index)
    {
        word.Init(wordsLeft[index], index, WordsCount);
        keyboard.SetActive(true);
    }
}
