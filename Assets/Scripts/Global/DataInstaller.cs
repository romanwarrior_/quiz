using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataInstaller")]
public class DataInstaller : ScriptableObject
{
    [SerializeField] private int minimalWordLength;
    [SerializeField] private int startAttemptsCount;

    public int MinimalWordLength => minimalWordLength;
    public int StartAttemptsCount => startAttemptsCount;
}
