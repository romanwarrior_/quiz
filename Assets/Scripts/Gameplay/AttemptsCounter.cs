using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AttemptsCounter : MonoBehaviour
{
    [SerializeField] private DataInstaller dataInstaller;
    [SerializeField] private TextMeshProUGUI text;

    private int attemptsLeft;

    private void Start()
    {
        GameEvents.Instance.OnAttemptLost.AddListener(OnAttemptLost);
        GameEvents.Instance.OnLevelCompleted.AddListener(OnGameFinished);
        GameEvents.Instance.OnLoadLevelClicked.AddListener(ResetAttempts);
        ResetAttempts();
    }

    private void OnAttemptLost()
    {
        attemptsLeft--;
        if (attemptsLeft <= 0)
        {
            GameEvents.Instance.OnLevelCompleted?.Invoke(false);
        }
        UpdateUI();
    }

    private void OnGameFinished(bool win)
    {
        if (win)
        {
            GameEvents.Instance.OnScoreGained?.Invoke(attemptsLeft);
        }
    }

    private void ResetAttempts(bool _ = true)
    {
        attemptsLeft = dataInstaller.StartAttemptsCount;
        UpdateUI();
    }

    private void UpdateUI()
    {
        text.text = $"Attempts left : {attemptsLeft}"; 
    }
}
