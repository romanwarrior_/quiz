using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AlphabetButton : MonoBehaviour
{
    [SerializeField] private char value;

    public void HandleClick()
    {
        GameEvents.Instance.OnButtonClicked?.Invoke(value);
        gameObject.SetActive(false);
    }
}
