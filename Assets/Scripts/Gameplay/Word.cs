using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Word : MonoBehaviour
{
    [SerializeField] private GameObject letter;

    private Letter[] letters;
    private string currentWord;
    private int lettersShown;
    private bool isLastWord;

    private readonly float[] spawnBorders = { -2 , 2};

    private void Start()
    {
        GameEvents.Instance.OnButtonClicked.AddListener(OnButtonClicked);
    }

    public void Init(string word, int wordIndex, int wordsCount)
    {
        currentWord = word;
        lettersShown = 0;
        isLastWord = wordIndex == wordsCount-1;

        if (letters!=null) DestroyLetters();
        SpawnLetters();
    }

    private void DestroyLetters()
    {
        foreach(var letter in letters)
        {
            Destroy(letter.gameObject);
        }
    }

    private void SpawnLetters()
    {
        letters = new Letter[currentWord.Length];

        int lettersCount = currentWord.Length;
        float wordWidth = spawnBorders[1] - spawnBorders[0];
        float letterWidth = wordWidth / (1.25f * lettersCount);
        float xSpacing = letterWidth / 4;
        Vector2 startSpawnpoint = new Vector2(spawnBorders[0] + letterWidth / 2, 0);

        for (int i = 0; i < lettersCount; i++)
        {
            letters[i] = Instantiate(letter, transform).GetComponent<Letter>();
            letters[i].transform.localScale = new Vector2(1, 1) * wordWidth / lettersCount * 0.8f;
            letters[i].transform.localPosition = startSpawnpoint + new Vector2(1, 0) * i * (letterWidth + xSpacing);
            letters[i].Init(this, currentWord[i]);
        }
    }

    private void OnButtonClicked(char letter)
    {
        if (currentWord.Count(letter) == 0)
        {
            GameEvents.Instance.OnAttemptLost?.Invoke();
        }
        lettersShown += currentWord.Count(letter);

        if (lettersShown == currentWord.Length)
        {
            if(!isLastWord) 
                GameEvents.Instance.OnLevelCompleted?.Invoke(true);
            else
                GameEvents.Instance.OnGameFinished?.Invoke();
        }
    }
}
