using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keyboard : MonoBehaviour
{
    [SerializeField] private GameObject[] keys;

    private void Start()
    {
        GameEvents.Instance.OnLoadLevelClicked.AddListener(EnableKeys);
    }

    private void EnableKeys(bool _ = true)
    {
        foreach (var key in keys)
        {
            key.SetActive(true);
        }
    }
}
