using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Letter : MonoBehaviour
{
    [SerializeField] private TextMeshPro text;

    private Word word;
    private char letter;

    private void Awake()
    {
        text.text = "";
        GameEvents.Instance.OnButtonClicked.AddListener(OnButtonClicked);
    }

    public void Init(Word word, char letter)
    {
        this.word = word;
        this.letter = letter;
    }

    private void OnButtonClicked(char value)
    {
        if (value == letter)
        {
            text.text = letter.ToString();
        }
    }
}
